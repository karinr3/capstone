$(document).ready(function() {
       var stickyNavTop = $('.navbar').offset().top;
       console.log("navbar");

       var stickyNav = function(){
     
        var scrollTop = $(window).scrollTop(); 
             
        if (scrollTop > stickyNavTop) { 
            $('.navbar').addClass('sticky');
        } else {
            $('.navbar').removeClass('sticky'); 
        }
    };

    stickyNav();
    $(window).scroll(function() {
        stickyNav();
    });
});


var donationData = [
    {
      logo:
        " <img src ='https://i2.wp.com/www.rencp.org/wp-content/uploads/2011/08/One-Laptop-Per-Child_Logo.jpg' width = '150' height= '70'/>",
      organization: "One Laptop Per Child",
      link: "<A HREF='http://one.laptop.org/' target='_blank'>One Laptop Per Child</A>"
    },
    {
        logo:
        " <img src ='https://www.mitinclusiveinnovation.com/wp-content/uploads/2018/08/NA_AnnieCannons_Logo.png' width = '150' height= '100'/>",
      organization: "Annie Cannons",
      link: "<A HREF='https://www.anniecannons.com/' target='_blank'>Annie Cannons</A>"
    },
    {
        logo:
        " <img src ='https://pbs.twimg.com/profile_images/471274589402578944/WDzinSti.png' width = '150' height= '100'/>",
      organization: "Beyond 12",
      link: "<A HREF='https://beyond12.org/what-we-do/'  target='_blank' >Beyond 12 </A>"
    },
  
    {
        logo:
        " <img src ='https://media.nbclosangeles.com/images/653*367/PowerMyLearning+Logo.jpg' width = '150' height= '70'/>",
      organization: "Power My Learning",
      link: "<A HREF='https://powermylearning.org/learn/get-involved/donate-technology/' target='_blank'>Power My Learning</A>"
    },
  ];
  
  function myFunction() {
    var listItems = document.querySelectorAll("#donations li");
    if (listItems.length > 0) {
      for (var i = 0; i < listItems.length; i++) {
        listItems[i].innerHTML = rowGen(donationData[i]);
      }
    }
  }
  
  function rowGen(x){
    var str = "";
    str += "<div class='row'>";
    str += "<div class='col-sm logo'>" + x.logo + "</div>";
    str += "<div class='col-sm school'>" + x.organization + "</div>";
    str += "<div class='col-sm conference'>" + x.link+ "</div>";
    str += "</div>";
    return str;
  }
  